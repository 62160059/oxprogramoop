
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ADMIN
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);
    char play;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please inupt Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            System.out.println("row " + row + "col: " + col);

            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty");
        }
    }

    public void showTurn() {
        System.out.println(table.getcurrentPlayer().getName() + " turn");
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (checkFinish()) {
                break;
            }
            table.switchPlayer();
        }

    }

    private boolean checkFinish() {
        if (table.isFinish()) {
            if (table.getWinner() == null) {
                this.showTable();
                System.out.println("Draw!!");
                System.out.println("Bye bye ...");
//                 this.Question();
            } else {
                this.showTable();
                System.out.println("Player " + table.getWinner().getName() + " Win!!");
                System.out.println("Bye bye ...");
//                this.Question();
            }

            return true;
        }

        return false;
    }

//    private void Question() {
//
//        while (true) {
//
//            System.out.println("Do you want to play again? (Y/N)");
//            play = kb.next().charAt(row);
//            if (play == 'Y') {
//                this.newGame();
//            }
//            if (play == 'N') {
//                break;
//            } else {
//
//            }
//        }
//    }

}
